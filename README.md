# なろう アクセス解析 ロガー

## これはなに？

小説家になろう の小説ごとのアクセス解析(※)の結果を Google SpreadSheet へ書き出します。

※ このアドレスのページです つ http://kasasagi.hinaproject.com/access/top/ncode/{NCODE}

## 準備

1. Google SpreadSheet の API キーを取得し、`service-account.json` という名前で保存
2. 新規 or 既存 の SpreadSheet を準備し、`config.json` へ `spreadsheet_id` と `sheet_id` を設定
3. 小説の NCODE を `config.json` へ設定
4. `php composer.phar install` を実行

## 設定

config.json を

```
{
	"spreadsheet_id": "PASTE HERE, GOOGLE SPREADSHEET ID",
	"sheet_id": "PASTE HERE, GOOGLE SPREADSHEET SHEET ID",
	"ncode": "PASTE HERE, NAROU SHOSETSU CODE"
}
```

のような感じで準備。

`spreadsheet_id` と `sheet_id` は スプレッドシートのアドレスの

```
https://docs.google.com/spreadsheets/d/{spreadsheet_id}/edit#gid={sheet_id}
```

から取得できます。

## 使い方

```
# php narou-access-logger.php
```

を一日一回定期的に実行してください。


## ライセンス

Copyright (c) 2016 Shun Sarumaru.

このソフトウエアは [The MIT License](https://opensource.org/licenses/mit-license.php) の下で提供されています。
