<?php

// Q. これはなに？
// A. 小説家になろう の小説ごとのアクセス解析(※)の結果を Google Spreadsheet へ書き出します。
//    ※ http://kasasagi.hinaproject.com/access/top/ncode/{NCODE}
//
// Q. 使い方は？
// A. Google Spreadsheet の API キーを取得して、諸々の設定を行ったら、
//    一日一回定期的に実行してください。

date_default_timezone_set('Asia/Tokyo');

if (function_exists('curl_init') &&
	!function_exists('curl_reset')) { // hack for under PHP 5.5
	function curl_reset(&$ch) {
		$ch = curl_init();
	}
}

require dirname(__FILE__).'/vendor/autoload.php';

require dirname(__FILE__).'/narou-functions.php';

function usage($message = false) {
	if ($message) {
		echo sprintf('ERROR %s'.PHP_EOL.PHP_EOL, $message);
	}
	echo sprintf('usage: php %s [CONFIG-JSON]'.PHP_EOL, basename(__FILE__));
	exit;
}

function get_($arr, $k, $def = false) {
	return isset($arr[$k]) ? $arr[$k] : $def;
}

//////////////////////////////////////////////////////////////////////

// 設定ファイル名を構築
$conf_json =
	$argc < 2 ? dirname(__FILE__).'/config.json'
	          : $argv[1];

if (NULL == ($config = json_decode(@file_get_contents($conf_json), true)) ||
	false === get_($config, 'spreadsheet_id') ||
	false === get_($config, 'sheet_id') ||
	false === get_($config, 'ncode') ) {
	// 設定ファイルがないなど、うまく読めなかった
	usage('config.json not found or invalid.');
}

define('DS', DIRECTORY_SEPARATOR);

// 認証情報のパスを設定
//   読み込み優先度
//   高い↓ 設定ファイルで指定
//   　　↓ service-account.json
//   低い↓ 読み込み失敗
$credential_file = get_($config, 'credential_file');
$credential_file = $credential_file ? realpath($credential_file) : false;
$credential_file = $credential_file ? $credential_file : dirname(__FILE__).'/service-account.json';

define('APPLICATION_NAME', 'Narou access dump');
define('CREDENTIAL_FILE',  $credential_file);
define('SPREADSHEETS_ID',  get_($config, 'spreadsheet_id'));
define('SHEET_ID',         get_($config, 'sheet_id'));
define('NCODE',            get_($config, 'ncode'));

define('DATE_COLUMN', 'A');

// 日時文字列から time_t へ
function timestamp_from_date($date, $fmt = 'Y/n/j') {
	$tmp = date_parse_from_format($fmt, $date);
	if (!$tmp['year']) {
		return false;
	}
	return mktime(intval($tmp['hour']),
	              intval($tmp['minute']),
	              intval($tmp['second']),
	              intval($tmp['month']),
	              intval($tmp['day']),
	              intval($tmp['year']));
}

// time_t から YMD への変換
function timestamp_to_YMD($timestamp) {
	return str_replace('/0', '/', date('Y/m/d', $timestamp));
}

// 日付文字列からスプレッドシートで利用する時刻のシリアル値に変換
function DateStrToSpreadSheetTimeSerial($date, $fmt = 'Y/n/j') {
	if (false === ($t = timestamp_from_date($date, $fmt))) {
		return 0;
	}
	// 元の文字列 2016/10/1
	// time_t     1475247600
	// 結果       1475247600/86400+25569.375
	return $t / 86400 + 25569.375; // ※ タイムゾーンは JST 固定
}

// スプレッドシートの参照形式から 行のインデックス(０ベース)を取得
function CellToRowIndex($cell) {
	if (!preg_match('/^[A-Z]+([0-9]+)$/', $cell, $m)) {
		return -1;
	}
	return intval($m[1]) - 1;
}

// スプレッドシートの参照形式から 列のインデックス(０ベース)を取得
function CellToColIndex($cell) {
	if (!preg_match('/^([A-Z]+)[0-9]+$/', $cell, $m)) {
		return -1;
	}
	$colIndex = 0;
	for ($i = 0, $num = strlen($m[1]), $ordA = ord('A'); $i < $num; $i++) {
		$colIndex = $colIndex * 26 + (ord($m[1][$i]) - $ordA + 1);
	}
	return $colIndex - 1;
}

// 行と列のインデックス(０ベース)からスプレッドシートの参照形式へ変換
function IndexToCell($rowIndex, $colIndex) {
	$col = '';
	for ($n = $colIndex, $sub = 0; !$sub || 0 < $n; $n = intval($n / 26), $sub = 1) {
		$col = chr(ord('A') + $n % 26 - $sub) . $col;
	}
	return '' . $col . ($rowIndex + 1);
}

// 挿入リクエストを生成
function create_insert_request($sheetId, $dimension, $offset)
{
	$from = is_array($offset) ? $offset[0] : $offset; 
	$to   = is_array($offset) ? $offset[1] : $offset;
	return [
		'insertDimension' => [
			'range' => [
				'sheetId'    => $sheetId,
				'dimension'  => $dimension,
				'startIndex' => 'ROWS' == $dimension ? CellToRowIndex($from)
				                                     : CellToColIndex($from),
				'endIndex'   => 'ROWS' == $dimension ? CellToRowIndex($to) + 1
				                                     : CellToColIndex($to) + 1
			]
		],
	];
}

// 更新リクエストを生成
function create_update_request($sheetId, $offset, $cells)
{
//print_r($cells);

	$rows = [ 'values' => [] ];

	foreach ($cells as $rowIdx => $row) {
		$values = [];
		foreach ($row as $colIdx => $col) {
			if (is_int($colIdx)) { // 値に従う
				if (is_numeric($col) && !is_string($col)) {
					$values[] = [ 'userEnteredValue' => ['numberValue' => $col] ];
				}
				else {
					$values[] = [ 'userEnteredValue' => ['stringValue' => $col] ];
				}
			}
			else { // 
				$value = $colIdx;
				if ('DATE' == $col && !is_numeric($value)) {
					// タイムシリアル値ではないなら直す
					// YYYY/MM/DD 固定
					$value = DateStrToSpreadSheetTimeSerial($value, 'Y/n/j');
				}
				$values[] = [ 'userEnteredValue' => [ 'numberValue' => $value ],
				              'userEnteredFormat'=> [ 'numberFormat' => [ 'type' => $col ] ]  ];
			}
		}
		$rows['values'] = $values;
	}
	return [
		'updateCells' => [
			'rows' => $rows,
			'fields' => '*',
			'start' => [
				'sheetId' => $sheetId,
				'rowIndex' => CellToRowIndex($offset),
				'columnIndex' => CellToColIndex($offset),
			]
		]
	];
}

//////////////////////////////////////////////////////////////////////

$context = stream_context_create([
  'http' => [
    'method' => "GET",
    'user_agent' => "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; Touch; rv:11.0) like Gecko"
  ]
]);

$url = 'http://kasasagi.hinaproject.com/access/top/ncode/'.NCODE.'/';
$data = file_get_contents($url, false, $context);
$data = str_replace("\r", "\n", str_replace("\r\n", "\n", $data));

// 取得した内容を解析

$info = narou_access_top_to_array($data);

// 念のためファイルに保存

$t = timestamp_from_date($info['update'], 'Y/m/d H:i');
$fname = dirname(__FILE__) . DS . 'html' . DS . NCODE . DS . date('Ym', $t) . DS . date('Ymd', $t) . '_' . date('Hi', $t) . '00_access.html';
@ mkdir (dirname($fname), 0777, true);
@ file_put_contents($fname, $data);

// Google spread sheets に書き込み

putenv('GOOGLE_APPLICATION_CREDENTIALS=' . CREDENTIAL_FILE);
$client = new Google_Client();
$client->useApplicationDefaultCredentials();
$client->addScope(Google_Service_Sheets::SPREADSHEETS);
$client->setApplicationName(APPLICATION_NAME);

$service = new Google_Service_Sheets($client);

// シート情報
$response = $service->spreadsheets->get(SPREADSHEETS_ID);
//var_dump($response);exit;
$sheetName = false;
$maxCols = -1;
for ($i = 0; $i < count($response->sheets); $i++) {
	$sheetProps = $response->sheets[$i]->getProperties();
	if (SHEET_ID == $sheetProps->getSheetId()) {
		$sheetName = $sheetProps->getTitle();
		$maxCols   = $sheetProps->getGridProperties()->getColumnCount();
		break;
	}
}

if (!$sheetName) {
	usage('sheet id not found');
}

define('SHEET_NAME', $sheetName);

// 更新位置を取得
$response = $service->spreadsheets_values->get(SPREADSHEETS_ID, SHEET_NAME.'!'.DATE_COLUMN.':'.DATE_COLUMN);
$target = [
	'yesterday' => [ 'timestamp' => -1, 'rowInddex' => -1, 'insert' => false ],
	'today'     => [ 'timestamp' => -1, 'rowInddex' => -1, 'insert' => false ]
];
$target['yesterday']['timestamp'] = timestamp_from_date($info['date']) - 24 * 60 * 60;
$target['today'    ]['timestamp'] = timestamp_from_date($info['date']);

for ($i = count($response['values']) - 1; 0 <= $i; $i--) {
	if (false == ($t = timestamp_from_date($response['values'][$i][0]))) {
		continue;
	}
	foreach (['yesterday', 'today'] as $pos) {
		if (-1 == $target[$pos]['rowInddex'] &&
			$t <= $target[$pos]['timestamp']) {
			$target[$pos]['insert']    = $t < $target[$pos]['timestamp'];
			$target[$pos]['rowInddex'] = $i + ($target[$pos]['insert'] ? 1 : 0);
		}
	}
}
if ($target['yesterday']['rowInddex'] < 0) {
	$target['yesterday']['rowInddex'] = 1;
}
if ($target['today']['rowInddex'] < 0) {
	$target['today']['rowInddex'] = $target['yesterday']['rowInddex'] + 1;
}

// 幅が足りなかったら、一番最初の書き込みと見做し初期化
if ($maxCols < 1 + 4 + 4 + 4 + 24) {
	// カラムを挿入
	$batchUpdateRequest = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
		'requests' => new Google_Service_Sheets_Request(
			create_insert_request(SHEET_ID, 'COLUMNS', [ 'A1', IndexToCell(1, 1 + 4 + 4 + 4 + 24 - $maxCols - 1) ])
		)
	]);
	$service->spreadsheets->batchUpdate(SPREADSHEETS_ID, $batchUpdateRequest);
	// ヘッダを作成
	$batchUpdateRequest = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
		'requests' => new Google_Service_Sheets_Request(
			create_update_request(SHEET_ID, 'A1', [[
				'日時',
				'累計PV',
				'累計PV(PC)', '累計PV(携帯)', '累計PV(スマホ)',
				'累計ユニーク',
				'累計ユニーク(PC)', '累計ユニーク(携帯)', '累計ユニーク(スマホ) ',
				'日計PV',
				'日計PV(PC)', '日計PV(携帯)', '日計PV(スマホ)',
				'日計PV(00時)', '日計PV(01時)', '日計PV(02時)', '日計PV(03時)',
				'日計PV(04時)', '日計PV(05時)', '日計PV(06時)', '日計PV(07時)',
				'日計PV(08時)', '日計PV(09時)', '日計PV(10時)', '日計PV(11時)',
				'日計PV(12時)', '日計PV(13時)', '日計PV(14時)', '日計PV(15時)',
				'日計PV(16時)', '日計PV(17時)', '日計PV(18時)', '日計PV(19時)',
				'日計PV(20時)', '日計PV(21時)', '日計PV(22時)', '日計PV(23時)'
			]])
		)
	]);
	$service->spreadsheets->batchUpdate(SPREADSHEETS_ID, $batchUpdateRequest);
}

// 上書きではない場合は行を挿入
if ($target['yesterday']['insert']) {
	$batchUpdateRequest = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
		'requests' => new Google_Service_Sheets_Request(
			create_insert_request(SHEET_ID, 'ROWS', DATE_COLUMN.($target['yesterday']['rowInddex'] + 1))
		)
	]);
	$service->spreadsheets->batchUpdate(SPREADSHEETS_ID, $batchUpdateRequest);
	$target['today']['rowInddex']++;
}
if ($target['today']['insert']) {
	$batchUpdateRequest = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
		'requests' => new Google_Service_Sheets_Request(
			create_insert_request(SHEET_ID, 'ROWS', DATE_COLUMN.($target['today']['rowInddex'] + 1))
		)
	]);
	$service->spreadsheets->batchUpdate(SPREADSHEETS_ID, $batchUpdateRequest);
}

// 更新

// カラム割り当て
//  A99日時
//  B99  累計PV
//  C99    累計PV(PC)
//  D99      累計PV(携帯)
//  E99        累計PV(スマホ)
//  F99          累計ユニーク
//  G99            累計ユニーク(PC)
//  H99              累計ユニーク(携帯)
//  I99                累計ユニーク(スマホ)
//      ※ここから前日分を反映
//  J99                  日計PV
//  K99                    日計PV(PC)
//  L99                      日計PV(携帯)
//  M99                        日計PV(スマホ)
//  N99                          日計PV(00時)
// AI99                            ... 日計PV(23時)


$batchUpdateRequest = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
	'requests' => new Google_Service_Sheets_Request(
		create_update_request(SHEET_ID, DATE_COLUMN.($target['today']['rowInddex'] + 1), [[
			timestamp_to_YMD($target['today']['timestamp']) => 'DATE',
			$info['total_access_total_pv'],
			$info['total_access_pc_pv'],
			$info['total_access_keitai_pv'],
			$info['total_access_smartphone_pv'],
			$info['total_access_total_uniq'],
			$info['total_access_pc_uniq'],
			$info['total_access_keitai_uniq'],
			$info['total_access_smartphone_uniq'],
		]])
	)
]);
$service->spreadsheets->batchUpdate(SPREADSHEETS_ID, $batchUpdateRequest);

$batchUpdateRequest = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
	'requests' => new Google_Service_Sheets_Request(
		create_update_request(SHEET_ID, DATE_COLUMN.($target['yesterday']['rowInddex'] + 1), [[
			timestamp_to_YMD($target['yesterday']['timestamp']) => 'DATE'
		]])
	)
]);
$service->spreadsheets->batchUpdate(SPREADSHEETS_ID, $batchUpdateRequest);

$batchUpdateRequest = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
	'requests' => new Google_Service_Sheets_Request(
		create_update_request(SHEET_ID, 'J'.($target['yesterday']['rowInddex'] + 1), [[
			$info['yesterday_pv_total'],
			$info['yesterday_pv_pc'],
			$info['yesterday_pv_keitai'],
			$info['yesterday_pv_smartphone'],
			$info['yesterday_pv_hour'][0], $info['yesterday_pv_hour'][1],
			$info['yesterday_pv_hour'][2], $info['yesterday_pv_hour'][3],
			$info['yesterday_pv_hour'][4], $info['yesterday_pv_hour'][5],
			$info['yesterday_pv_hour'][6], $info['yesterday_pv_hour'][7],
			$info['yesterday_pv_hour'][8], $info['yesterday_pv_hour'][9],
			$info['yesterday_pv_hour'][10],$info['yesterday_pv_hour'][11],
			$info['yesterday_pv_hour'][12],$info['yesterday_pv_hour'][13],
			$info['yesterday_pv_hour'][14],$info['yesterday_pv_hour'][15],
			$info['yesterday_pv_hour'][16],$info['yesterday_pv_hour'][17],
			$info['yesterday_pv_hour'][18],$info['yesterday_pv_hour'][19],
			$info['yesterday_pv_hour'][20],$info['yesterday_pv_hour'][21],
			$info['yesterday_pv_hour'][22],$info['yesterday_pv_hour'][23],
		]])
	)
]);
$service->spreadsheets->batchUpdate(SPREADSHEETS_ID, $batchUpdateRequest);


