<?php

function narou_access_top_to_array($data)
{
	$info = [];
	
	// 日付
	if (preg_match('@<div id="today_yesterday_data">.*?<p class="novelview_h3">' .
	                '.+?\(([0-9]{4})年([0-9]{2})月([0-9]{2})日\).+?' .
	                '</p>.*?<div id="today_data">@s', $data, $m))
	{
		$info['date'] = str_replace('/0', '/', $m[1] . '/' . $m[2] . '/' . $m[3]);
	}
	
	// 更新日時
	if (preg_match('@<div class="last">.*?' .
	                '([0-9]+?)[^0-9]+([0-9]+?)[^0-9]+([0-9]+?)[^0-9]+([0-9]+?)[^0-9]+([0-9]+?)[^0-9]+?' .
	                '.+?<!-- access_infomation -->@s', $data, $m))
	{
		$info['update'] = preg_replace('@([/: ])0@', '$1', sprintf('%s/%s/%s %s:%s', $m[1], $m[2], $m[3], $m[4], $m[5]));
	}
	
	// 昨日のPV
	if (preg_match('@<div id="yesterday_data">.+?<div class="oneday_access_table">' .
	                '(.+?)' .
	                '<div class="oneday_graph">(.+?)</div><!-- yesterday_data -->@s', $data, $m))
	{
		preg_match_all('@<td.+?>([0-9,]+)@s', $m[1], $m1);
		preg_match_all('@<td class="pv">([0-9,]+)@s', $m[2], $m2);
	
		// $m1[0] 累計PV
		// $m1[1] パソコンPV
		// $m1[2] 携帯PV
		// $m1[3] スマートフォンPV
		$info['yesterday_pv_total']      = intval(str_replace(',', '', $m1[1][0]));
		$info['yesterday_pv_pc']         = intval(str_replace(',', '', $m1[1][1]));
		$info['yesterday_pv_keitai']     = intval(str_replace(',', '', $m1[1][2]));
		$info['yesterday_pv_smartphone'] = intval(str_replace(',', '', $m1[1][3]));
	
		// $m2[0]  当日 00時 PV
		//   :
		// $m2[23] 当日 23時 PV
		$info['yesterday_pv_hour'] = []; // 念のため
		for ($i = 0; $i < 24; $i++) {
			$info['yesterday_pv_hour'][] = intval(str_replace(',', '', $m2[1][$i]));
		}
	}
	
	// 累計PVなど
	if (preg_match('@<div class="total_access_table">(.+?)<!-- total_access_table -->@s', $data, $m))
	{
		preg_match_all('@<td.+?>([0-9,]+)@s', $m[1], $mm);
	
		// [0] 累計PV
		// [1] 累計ユニーク
		// [2] パソコンPV
		// [3] パソコンユニーク
		// [4] 携帯PV
		// [5] 携帯ユニーク
		// [6] スマートフォンPV
		// [7] スマートフォンユニーク
		$info['total_access_total_pv']        = intval(str_replace(',', '', $mm[1][0]));
		$info['total_access_total_uniq']      = intval(str_replace(',', '', $mm[1][1]));
		$info['total_access_pc_pv']           = intval(str_replace(',', '', $mm[1][2]));
		$info['total_access_pc_uniq']         = intval(str_replace(',', '', $mm[1][3]));
		$info['total_access_keitai_pv']       = intval(str_replace(',', '', $mm[1][4]));
		$info['total_access_keitai_uniq']     = intval(str_replace(',', '', $mm[1][5]));
		$info['total_access_smartphone_pv']   = intval(str_replace(',', '', $mm[1][6]));
		$info['total_access_smartphone_uniq'] = intval(str_replace(',', '', $mm[1][7]));
	}

	return $info;
}
